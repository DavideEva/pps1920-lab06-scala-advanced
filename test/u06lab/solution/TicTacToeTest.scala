package u06lab.solution

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u06lab.solution.TicTacToe._

class TicTacToeTest {

  @Test
  def findTest(): Unit ={
    assertEquals(Some(X), find(List(Mark(0,0,X)),0,0))
    assertEquals(Some(O), find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1))
    assertEquals(None, find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1))
  }

  @Test
  def boardHaveTrisTest(): Unit ={
    assertTrue(boardHaveTris(List(Mark(0,0,X),Mark(0,1,X),Mark(0,2,X))))
    assertFalse(boardHaveTris(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X))))
    assertTrue(boardHaveTris(List(Mark(0,0,X),Mark(1,1,X),Mark(2,2,X))))
    assertFalse(boardHaveTris(List(Mark(0,0,X),Mark(1,1,O),Mark(2,2,X))))
  }

  @Test
  def placeAnyMarkTest(): Unit ={
    assertEquals(9, placeAnyMark(List(), X).size)
    assertEquals(8, placeAnyMark(List(Mark(0,0,O)), X).size)
  }

  @Test
  def computeAnyGameTest(): Unit ={
    assertEquals(9*8*7*6, computeAnyGame(O, 4).map(_.head).distinct.size)
    assertEquals(9*8*7*6*5*4*3*2*1, computeAnyGame(O, 9).map(_.head).distinct.size)
  }

  @Test
  def computeAnyGameWithStopTest(): Unit = {
    assertEquals(9*8*7*6, computeAnyGameWithStop(O, 4).map(_.head).distinct.size)
    assertEquals((9*8*7*6*5 - 1440) * 4 + 1440, computeAnyGameWithStop(O, 6).map(_.head).distinct.size)
    assertEquals(255168, computeAnyGameWithStop(O, 9).map(_.head).distinct.size)
  }
}
