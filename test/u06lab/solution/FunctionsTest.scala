package u06lab.solution

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class FunctionsTest {

    val f1: Functions = FunctionsImpl
    val f2: Functions = FunctionsImpl2

    @Test
    def sumTest() {
        assertEquals(f1.sum(List(10.0,20.0,30.1)), 60.1)
        assertEquals(f2.sum(List(10.0,20.0,30.1)), 60.1)
        assertEquals(f1.sum(List()), 0.0)
        assertEquals(f2.sum(List()), 0.0)
    }

    @Test
    def concatTest() {
        assertEquals(f1.concat(Seq("a","b","c")), "abc")
        assertEquals(f2.concat(Seq("a","b","c")), "abc")
        assertEquals(f1.concat(Seq()), "")
        assertEquals(f2.concat(Seq()), "")
    }

    @Test
    def maxTest() {
        assertEquals(f1.max(List(-10,3,-5,0)), 3)
        assertEquals(f2.max(List(-10,3,-5,0)), 3)
        assertEquals(f1.max(List()), -2147483648)
        assertEquals(f2.max(List()), -2147483648)
    }
}