package u06lab.solution

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class ParserTest {

  @Test
  def parserTest() {
    def parser = new BasicParser(Set('a', 'b', 'c'))

    assertTrue(parser.parseAll("aabc".toList))
    assertFalse(parser.parseAll("aabcdc".toList))
    assertTrue(parser.parseAll("".toList))
  }

  @Test
  def nEParserTest() {
    // Note NonEmpty being "stacked" on to a concrete class
    // Bottom-up decorations: NonEmptyParser -> NonEmpty -> BasicParser -> Parser
    def parserNE = new NonEmptyParser(Set('0', '1'))

    assertTrue(parserNE.parseAll("0101".toList)) // true
    assertFalse(parserNE.parseAll("0123".toList)) // false
    assertFalse(parserNE.parseAll(List())) // false
  }

  @Test
  def nTCarserTest() {
    def parserNTC = new NotTwoConsecutiveParser(Set('X', 'Y', 'Z'))

    assertTrue(parserNTC.parseAll("XYZ".toList)) // true
    assertFalse(parserNTC.parseAll("XYYZ".toList)) // false
    assertTrue(parserNTC.parseAll("".toList)) // true
  }

  @Test
  def nTCNEarserTest() {
    // note we do not need a class name here, we use the structural type
    def parserNTCNE = new BasicParser(Set('X', 'Y', 'Z')) with NotTwoConsecutive[Char] with NonEmpty[Char]

    assertTrue(parserNTCNE.parseAll("XYZ".toList)) // true
    assertFalse(parserNTCNE.parseAll("XYYZ".toList)) // false
    assertFalse(parserNTCNE.parseAll("".toList)) // false
  }

  @Test
  def sParserTest() {
    import ImplicitParser._

    def sparser : Parser[Char] = "abc".charParser()
    assertTrue(sparser.parseAll("aabc".toList)) // true
    assertFalse(sparser.parseAll("aabcdc".toList)) // false
    assertTrue(sparser.parseAll("".toList)) // true
  }
}
