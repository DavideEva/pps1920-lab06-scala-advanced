package u06lab.solution

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board find (e => e.x == x && e.y == y) map (_.player)

  def getCol(board: Board, y: Int): Traversable[Player] = board filter (_.y == y) map (_.player)

  def getRow(board: Board, x: Int): Traversable[Player] = board filter (_.x == x) map (_.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    for {
      x <- 0 to 2
      y <- 0 to 2
      if find(board, x, y).isEmpty
    } yield Mark(x, y, player) :: board
  }

  private def checkTris[A](elems: Traversable[A]): Boolean = (elems.forall(_ == X) || elems.forall(_ == O)) && elems.size == 3

  private def checkRow(board: Board): Boolean = {
    {
      for (rowIdx <- 0 to 2) yield checkTris(getRow(board, rowIdx))
    } contains true
  }

  private def checkCol(board: Board): Boolean = {
    {
      for (colIdx <- 0 to 2) yield checkTris(getCol(board, colIdx))
    } contains true
  }

  private def checkDiagonal(board: Board): Boolean = {
    // check first diagonal
    checkTris(board filter (b => b.x == b.y) map (_.player)) ||
      // check second diagonal
      checkTris(board filter (b => b.x + b.y == 2) map (_.player))
  }

  def boardHaveTris(board: Board): Boolean = {
    checkDiagonal(board) || checkCol(board) || checkRow(board)
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case _ => for {
      game <- computeAnyGame(player.other, moves - 1)
      moves <- placeAnyMark(game.head, player)
    } yield moves :: game
  }

  def computeAnyGameWithStop(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case n => for {
      game <- computeAnyGameWithStop(player.other, n - 1)
      nextMove <- placeAnyMark(game.head, player)
    } yield if (!boardHaveTris(game.head)) nextMove :: game else game
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) {
        print(" "); if (board == game.head) println()
      }
    }

  // Exercise 1: implement find such that...
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  // Exercise 2: implement placeAnyMark such that...
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  //computeAnyGame(O, 1) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one to stop each game when someone won!!
  computeAnyGameWithStop(O, 2) foreach { g => printBoards(g); println() }
}